//console.log("Hello World");
//  DISCUSSION #1
					//querySelector - will get the id of document
const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

/*
// Using "keyup" event
txtFirstName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtFirstName.value;

})

// for checking para alam mo kung saan ginagalaw yung object mo

txtFirstName.addEventListener('keyup', (event) => {
	// The document where our JS is connected
	console.log(event.target);
	// The value that our "keyup" event listened
	console.log(event.target.value);
	
})
*/

// DISCUSSION #2

const updateFullName = () => {
	let firstName = txtFirstName.value;
	let lastName = txtLastName.value;

	//innerHTML -content mo sa html na pumapasok
	spanFullName.innerHTML = `${firstName} ${lastName}`;
} 

txtFirstName.addEventListener('keyup', updateFullName);
txtLastName.addEventListener('keyup', updateFullName);